import React, { Component } from 'react';
import { List, Card, Typography, Avatar, Button, Icon, message } from 'antd';
import Header from '../components/general/HeaderMain';
import { Element, animateScroll as scroll } from "react-scroll";
import axios from 'axios'
const { Title } = Typography;

class ManagePostPage extends Component {

    state = {
        imageUrl: 'https://sv1.picz.in.th/images/2019/06/12/1tZYEN.png',
        email: 'Athena Nicasio ',
        blogs: []
    }

    componentDidMount() {
        const jsonStr = localStorage.getItem('user-data');
        const user = jsonStr && JSON.parse(jsonStr)
        if (user.role === 'member') {
            this.props.history.push("/post")
        }
        axios.get("http://localhost:8080/all/blog").then(res => {
            if (res.data) {
                let data = res.data
                this.setState({ blogs: data })
            } else {
                console.log("data not found")
            }
        })
            .catch(error => {
                console.log(error);
            });
    }

    onClickPost = (blogId) => {
        //TODO go to page post 
        this.props.history.push("/blog", { id: blogId })
    }

    onClickDeleteButton = (blogId, userId) => {
        if (blogId && userId) {
            axios({
                url: `http://localhost:8080/delete/blog/${blogId}/${userId}`,
                method: "delete"
            }).then((response) => {
                message.success('ลบสำเร็จ', 1);
                axios.get("http://localhost:8080/all/blog").then(res => {
                    if (res.data) {
                        let data = res.data
                        this.setState({ blogs: data })
                    } else {
                        console.log("data not found")
                    }
                })
                    .catch(error => {
                        console.log(error);
                    });
            }).catch((error) => {
                message.error('ลบไม่สำเร็จ', 1);
            })
        } else {
            console.log("id not foud")
        }
    }

    render() {
        return (
            <div>
                <Header />
                <div style={{ marginTop: 50, marginBottom: 50, marginLeft: 80, marginRight: 80 }}>
                    <div style={{ flex: 1, justifyContent: 'flex-start', display: 'flex', paddingLeft: 100 }}>
                        <Title level={2}>Manage Post</Title>
                    </div>
                    <div style={{ borderStyle: "solid", borderWidth: "1px", borderColor: "black", margin: 80, borderRadius: 50 }}>
                        <Element style={{ position: "relative", height: "800px", overflow: "scroll", marginBottom: "100px", margin: "50px" }}>
                            <List
                                itemLayout="horizontal"
                                dataSource={this.state.blogs}
                                renderItem={item => (
                                    <div style={{ paddingTop: "50px", marginLeft: 100, marginRight: 100, justifyContent: 'left', display: 'flex' }}>
                                        <Card
                                            style={{ width: "100%", borderStyle: "solid", borderWidth: "1px", borderColor: "black", borderRadius: 50, marginBottom: 50 }}
                                        >
                                            <List.Item>
                                                <List.Item.Meta
                                                    avatar={item.userData.image === null ?
                                                        <img alt="example" src="https://sv1.picz.in.th/images/2019/06/12/1tZYEN.png" height={140} width={140} style={{ borderRadius: 100 }} /> :
                                                        <img alt="example" src={item.image} height={140} width={140} style={{ borderRadius: 100 }} />}
                                                    title={<h2 style={{ marginTop: 15 }}>ชื่อ ผู้ใช้ : {item.userData.name}</h2>}
                                                    description={
                                                        <div style={{ flexDirection: 'row', display: 'flex' }}>
                                                            <div style={{ width: 200 }}>
                                                                <label>เรื่องที่โพส : {item.detail} </label>
                                                                <p>ประเภท : {item.blogType.typeName} </p>
                                                            </div>
                                                            <div style={{ width: '100%', justifyContent: 'flex-end', display: 'flex' }}>
                                                                <Button
                                                                    type="primary"
                                                                    size="large"
                                                                    style={{ margin: 20, backgroundColor: '#2FEB3C', borderColor: '#2FEB3C', borderRadius: 50, height: 44, width: 90 }}
                                                                    onClick={() => { this.onClickPost(item.id) }}
                                                                >
                                                                    เปิด
                                                                    </Button>
                                                                <Button
                                                                    onClick={() => { this.onClickDeleteButton(item.id, item.userData.id) }}
                                                                    type="primary"
                                                                    size="large"
                                                                    style={{ margin: 20, backgroundColor: '#ff0000', borderColor: '#ff0000', borderRadius: 50, height: 44, width: 90 }}
                                                                >
                                                                    <Icon type="delete" /> ลบ
                                                                    </Button>
                                                            </div>
                                                        </div>
                                                    }
                                                />
                                            </List.Item>
                                        </Card>
                                    </div>
                                )}
                            />
                        </Element>

                    </div>
                </div>
            </div>
        )
    }

}

export default ManagePostPage;
