import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import MainPage from '../pages/MainPage';
import ProfilePage from '../pages/ProfilePage';

function RouteMenu(props) {
    return (
        <div style={{ width: '100%' }}>
            <Switch>
                <Route exact path="/main" component={MainPage} />
                <Route path="/profile" exact component={ProfilePage} />
                <Redirect from="*" exact to="/" />
            </Switch>
        </div>
    )
}

export default RouteMenu