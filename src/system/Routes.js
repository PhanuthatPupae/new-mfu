import React from 'react'
import { Route, Switch } from 'react-router-dom'
import LoginPage from '../pages/LoginPage'
import HomePage from '../pages/HomePage'
import MainPage from '../pages/MainPage'
import RegisterPage from '../pages/RegisterPage'
import BlogPage from '../pages/BlogPage'
import ManagePostPage from '../pages/ManagePostPage';
import ManageKnowledgePage from '../pages/ManageKnowledgePage'
import ManageAdminPage from '../pages/ManageAdminPage';
import ProfilePage from '../pages/ProfilePage';
import PostPage from '../pages/PostPage';
import ChartPage from '../pages/ChartPage';
import AddBlogPage from '../pages/AddBlogPage';
import EditBlogPage from '../pages/EditBlogPage';

function Routes() {
    return (
        <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/register" exact component={RegisterPage} />
            <Route path="/login" exact component={LoginPage} />
            <Route path="/main" exact component={MainPage} />
            <Route path="/profile" exact component={ProfilePage} />
            <Route path="/post" exact component={PostPage} />
            <Route path="/managepost" exact component={ManagePostPage} />
            <Route path="/manageknowledge" exact component={ManageKnowledgePage} />
            <Route path="/manageadmin" exact component={ManageAdminPage} />
            <Route path="/blog" exact component={BlogPage} />
            <Route path="/chart" exact component={ChartPage} />
            <Route path="/addblog" exact component={AddBlogPage} />
            <Route path="/editblog" exact component={EditBlogPage} />
            <Route component={HomePage} />
        </Switch>
    )
}

export default Routes